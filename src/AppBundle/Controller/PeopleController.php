<?php

namespace AppBundle\Controller;

use AppBundle\Entity\People;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * People controller.
 *
 * @Route("/")
 */
class PeopleController extends Controller
{
    /**
     * Creates a new person entity.
     *
     * @Route("/", name="people_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $person = new People();
        $form = $this->createForm('AppBundle\Form\PeopleType', $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($person);
            $em->flush();
            $this->addFlash(
                'notice',
                'Register Success'
            );
            return $this->redirectToRoute('people_new');
        }

        return $this->render('people/new.html.twig', array(
            'person' => $person,
            'form' => $form->createView(),
        ));
    }
}
