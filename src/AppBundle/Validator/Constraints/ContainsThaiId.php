<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsThaiId extends Constraint
{
    public $message = 'The Thai ID "{{ string }}" is invalid.';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}