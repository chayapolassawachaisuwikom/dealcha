<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsThaiIdValidator extends ConstraintValidator
{
    public function validate($personID, Constraint $constraint)
    {
        if (strlen($personID) != 13) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $personID)
                ->addViolation();
            return;
        }

        $rev = strrev($personID);
        $total = 0;
        for($i=1;$i<13;$i++)
        {
            $mul = $i +1;
            $count = $rev[$i]*$mul;
            $total = $total + $count;
        }
        $mod = $total % 11;
        $sub = 11 - $mod;
        $check_digit = $sub % 10;

        if(!($rev[0] == $check_digit)){
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $personID)
                ->addViolation();
        }
    }
}