<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use AppBundle\Entity\People;

class PeopleAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text');
        $formMapper->add('lastName', 'text');
        $formMapper->add('thaiId', 'text');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        $datagridMapper->add('lastName');
        $datagridMapper->add('thaiId');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('lastName');
        $listMapper->addIdentifier('thaiId');
    }

    public function toString($object)
    {
        return $object instanceof People
            ? $object->getName(). ' '. $object->getLastName()
            : 'People'; // shown in the breadcrumb on the create view
    }
}