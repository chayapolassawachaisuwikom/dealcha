### Installation

```sh
composer install
bower install ./vendor/sonata-project/admin-bundle/bower.json
bower install

php app/console doctrine:database:create
php app/console doctrine:schema:update --force

php app/console server:start
```

### URL
http://127.0.0.1:8000/
for Control panel
http://127.0.0.1:8000/admin